package com.example.flickrapp.Controller;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.widget.ListView;

import com.example.flickrapp.R;

public class ListActivity extends AppCompatActivity {
    ListView lv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        allowGeoloc(); // Exercise 33

        MyAdapter myAdapter = new MyAdapter(this);
        lv = ((ListView)findViewById(R.id.lv_list));

        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                AsyncFlickrJSONDataForList asyncFlickerJSONDataForList = new AsyncFlickrJSONDataForList(ListActivity.this, myAdapter);
                asyncFlickerJSONDataForList.onPostExecute(asyncFlickerJSONDataForList.doInBackground("https://www.flickr.com/services/feeds/photos_public.gne?tags=trees&format=json"));
                changeListView(myAdapter);
            }
        });
        t.start();
    }

    public void changeListView(MyAdapter adapter) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // Binds the Adapter to the ListView
                lv.setAdapter(adapter);
            }
        });
    }

    public void allowGeoloc(){
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions( this, new String[] {  android.Manifest.permission.ACCESS_COARSE_LOCATION  }, 48 );
        }
    }
}