package com.example.flickrapp.Controller;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.flickrapp.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        allowGeoloc(); // Exercise 33

        ((Button)findViewById(R.id.b_getImage)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Thread t = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        AsyncFlickrJSONData asyncFlickerJSONData = new AsyncFlickrJSONData(MainActivity.this);
                        asyncFlickerJSONData.onPostExecute(asyncFlickerJSONData.doInBackground("https://www.flickr.com/services/feeds/photos_public.gne?tags=trees&format=json"));
                    }
                });
                t.start();
            }
        });

        ((Button)findViewById(R.id.b_goToList)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent listActivity = new Intent(MainActivity.this, ListActivity.class);
                startActivity(listActivity);
            }
        });

    }

    public void allowGeoloc(){
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions( this, new String[] {  android.Manifest.permission.ACCESS_COARSE_LOCATION  }, 48 );
        }
    }
}