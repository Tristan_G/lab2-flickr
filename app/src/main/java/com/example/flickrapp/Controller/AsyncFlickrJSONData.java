package com.example.flickrapp.Controller;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.flickrapp.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class AsyncFlickrJSONData extends AsyncTask<String, Void, JSONObject> {

    private AppCompatActivity myActivity;

    public AsyncFlickrJSONData(AppCompatActivity mainActivity) {
        myActivity = mainActivity;
    }

    @Override
    protected JSONObject doInBackground(String... strings) {

        URL url = null;
        HttpURLConnection urlConnection = null;
        String result = null;
        try {
            url = new URL(strings[0]);
            urlConnection = (HttpURLConnection) url.openConnection(); // Open
            InputStream in = new BufferedInputStream(urlConnection.getInputStream()); // Stream

            result = readStream(in); // Read stream
        }
        catch (MalformedURLException e) { e.printStackTrace(); }
        catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            if (urlConnection != null)
                urlConnection.disconnect();
        }

        JSONObject json = null;
        try {
            json = new JSONObject(result);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        return json; // returns the result
    }

    private String readStream(InputStream is) throws IOException {
        StringBuilder sb = new StringBuilder();
        BufferedReader r = new BufferedReader(new InputStreamReader(is),1000);
        for (String line = r.readLine(); line != null; line =r.readLine()){
            sb.append(line);
        }
        is.close();

        String res = sb.substring("jsonFlickrFeed(".length(),sb.length()-1); // Extract

        return res;
    }

    @Override
    protected void onPostExecute(JSONObject s) {
        try {
            JSONArray items = s.getJSONArray("items");
            JSONObject flickr_entry = items.getJSONObject(0);
            String urlmedia = flickr_entry.getJSONObject("media").getString("m");
            Log.i("CIO", "URL media: " + urlmedia);

            // Downloading image
            AsyncBitmapDownloader abd = new AsyncBitmapDownloader();
            changeImageByView(abd.doInBackground(urlmedia));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void changeImageByView(Bitmap bm){
        ImageView iv = ((ImageView)myActivity.findViewById(R.id.iv_image));
        myActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                iv.setImageBitmap(bm);
            }
        });
    }
}
