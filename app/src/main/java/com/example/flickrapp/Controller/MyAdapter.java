package com.example.flickrapp.Controller;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.ImageRequest;
import com.example.flickrapp.Model.MySingleton;
import com.example.flickrapp.R;

import java.util.Vector;

public class MyAdapter extends BaseAdapter {
    private Vector<String> urlStorage;
    private Context context;

    public MyAdapter(Context context){
        urlStorage = new Vector<String>();
        this.context = context;
    }

    @Override
    public int getCount() {
        return urlStorage.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = LayoutInflater.from(context)
                    //.inflate(R.layout.texte, parent, false); // Exercise 25
                    .inflate(R.layout.bitmaplayout, parent, false); // Exercise 28,29 and 30

        }

        // Exercise 25
        //TextView tv = (TextView)convertView.findViewById(R.id.tv_list);
        //tv.setText(urlStorage.get(position));

        // Exercise 28,29 and 30
        ImageView iv = (ImageView)convertView.findViewById(R.id.iv_listImages);

        RequestQueue queue = MySingleton.getInstance(parent.getContext()).getRequestQueue();
        Response.Listener<Bitmap> rep_listener = response -> {
            iv.setImageBitmap(response);
        };
        Response.ErrorListener errorListener = response -> {
            Log.i("ERROR", "Can't download this image");
        };

        ImageRequest request = new ImageRequest(
                urlStorage.get(position), rep_listener, 2000,
                2000, ImageView.ScaleType.CENTER, Bitmap.Config.RGB_565, errorListener);

        queue.add(request);

        return convertView;
    }

    public void add(String url){
        urlStorage.add(url);
        Log.i("JFL", "Adding to adapter url : " + url);
    }
}
